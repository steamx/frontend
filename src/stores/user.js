import { defineStore } from 'pinia'
import { ref, computed, watch } from 'vue'
import axios from 'axios'

export const useUserStore = defineStore('jwt', () => {
  const jwt = ref(undefined)

  const userDetails = ref(undefined)

  const userId = computed(() => {
    if (!jwt.value) {
      return ''
    }

    const decodedJwtPayload = JSON.parse(atob(jwt.value.split('.')[1]))

    return decodedJwtPayload.userSteamId ?? ''
  })

  watch(userId, async () => {
    if (!jwt.value || !userId.value) {
      userDetails.value = undefined
    }

    const gatewayUrl = import.meta.env.VITE_GATEWAY_URL

    const url = `${gatewayUrl}/steam-api/getUserDetails`
    const axiosConfig = {
      headers: {
        Authorization: `Bearer ${jwt.value}`
      }
    }

    userDetails.value = (await axios.get(url, axiosConfig)).data
  })

  return { jwt, userId, userDetails }
})
