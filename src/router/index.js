import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  mode: 'history',
  routes: [
    {
      path: '/user',
      name: 'user-profile',
      component: () => import('../views/UserProfile.vue')
    },
    {
      path: '/game/:id',
      name: 'game-details',
      component: () => import('../views/GameDetails.vue')
    },
    {
      path: '/',
      name: 'home',
      component: () => import('../views/Home.vue')
    },
    {
      path: '/steam-auth',
      name: 'steam-auth',
      component: () => import('../views/SteamAuth.vue')
    }
  ]
})

export default router
