# Frontend

## About
The frontend application is written in vue.js 3.0 with Vite. It utilizes the vuetify component library/css library.

## Views
### Home
Main view of the application where user can log in and search for games on steam platform.

### Game details
On this view logged in user can see the details of searched game from home view. He can display game's title, description, reviews, genre, requirements and more.

### User profile
The user can check games that belongs to user on steam platform and view their details.

### Steam auth
The view behaves like a proxy between steam openID 2.0 and auth service to log in user. After successful login the JWT token is saved in pinia store.
## How to run
`docker build -t frontend .`
`docker run -p 8080:80 frontend`

To run whole steamx application check `Infrastructure`
repository
## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
